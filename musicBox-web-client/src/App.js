import React from 'react'
import { Route, Link, Switch } from 'react-router-dom'
import Login from './containers/Login'
import Favorites from './containers/Favorites'
import Buffer from "./containers/Buffer";

export default class App extends React.Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-dark bg-primary nav__box">
                    <a className="navbar-brand" href="/">
                        <img src="https://png.icons8.com/metro/1600/microphone.png" width="40" height="40" className="d-inline-block align-top" alt="" />
                            <span className="nav__logo">MusicBox</span>
                    </a>
                    <ul className="nav justify-content-end">
                        <li className="nav-item">
                            <Link className="nav-link" to="/login">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/albums">Albums</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link disabled" to="/favorites">Favorites</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route path="/albums" component={Buffer} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/favorites" component={Favorites} />
                </Switch>
            </div>
        )
    }
}
