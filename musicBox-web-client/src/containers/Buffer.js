import React from 'react'
import Switch from "react-router-dom/es/Switch";
import Route from "react-router-dom/es/Route";
import OpenAlbum from "./OpenAlbum";
import Albums from "./Albums";

export default class Buffer extends React.Component {

    render() {
        const {match} = this.props;
        return (
            <div>
                <Switch>
                    <Route exact path={match.path} render={() => <Albums />} />
                    <Route path={`${this.props.match.path}/:id/`} render={props => <OpenAlbum id={props.match.params.id}/>} />
                </Switch>
            </div>
        )
    }
}
