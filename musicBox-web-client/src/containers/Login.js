import React from 'react'
import {Field, reduxForm} from 'redux-form'

const checkForm = form => {
    console.log(form.login)
    console.log(form.password)
}

const Login = (props) => {
    return (
        <form onSubmit={props.handleSubmit(checkForm)}>
            <div>
                <label htmlFor="login">Login</label>
                <Field name="login" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <Field name="password" component="input" type="password" />
            </div>
            <button type="submit">Login</button>
            <button type="button" onClick={props.reset}>Reset</button>
        </form>
    )
}

export default reduxForm({form: 'auth'})(Login)

