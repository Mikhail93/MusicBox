import React from 'react'

export default class OpenAlbum extends React.Component {

    constructor(params) {
        super(params)

        this.state = {
            album: {},
            tracks: []
        }

        this.getAlbum();
    }

    getAlbum = () => {
        if (this.state.tracks.length === 0) {
            fetch('/api/albums/' + this.props.id)
                .then(res => res.json())
                .then(data => this.setState({album: data}))
                .then(() => this.setState({tracks: this.state.album.tracks.map(track =>
                    <div className="card" style="width: 18rem;">
                        <img className="card-img-top" src="..." alt="Card image cap" />
                            <div className="card-body">
                                <h5 className="card-title">{track.title}</h5>
                                <a href="#" className="btn btn-primary">Go somewhere</a>
                            </div>
                    </div>
                )}))
        }
    }

    render() {
        return (
            <div>
                /*<img className="album__cover__tracks" src={this.state.album.cover} />*/
                {this.state.tracks}
            </div>
        )

    }
}