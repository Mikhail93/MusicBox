import React from 'react'
import {Link} from "react-router-dom";

export default class Albums extends React.Component {

    constructor(params) {
        super(params)

        this.state = {
            albums: []
        }
    }

    componentDidMount() {
        if (this.state.albums.length === 0) {
            fetch('/api/albums/')
                .then(res => res.json())
                .then(data => this.setState({albums: data}))
        }
    }

    render() {
        return (
            <div>
                <p className="hello_world">All albums</p>
                    {this.state.albums.map(album =>
                        <div className="album__box" key={album.title}>
                            <Link to={"/albums/" + album.id}>
                                <img className="album__cover" src={album.cover} />
                                <div className="vision">
                                    <p className="album__title">{album.title}</p>
                                    <p className="album__artist">{album.artist}</p>
                                </div>
                            </Link>
                        </div>
                    )}
            </div>
        )
    }
}
