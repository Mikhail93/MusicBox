package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.model.Album;
import ua.danit.service.AlbumService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api/albums")
public class AlbumsController {

  @Autowired
  private AlbumService albumService;

  @GetMapping(path = "/")
  public Iterable<Album> getAllAlbums() {
    return albumService.getAllAlbums();
  }

  @GetMapping(path = "/{id}")
  public Album getAlbumById(@PathVariable Long id) {
    return albumService.getAlbumById(id);
  }

  @DeleteMapping(path = "/{id}")
  public void deleteAlbum(@PathVariable Long id) {
    albumService.deleteAlbum(id);
  }

  @DeleteMapping(path = "/{id}/cover")
  public void deleteCover(@PathVariable Long id) {
    albumService.deleteCover(id);
  }

  @PutMapping(value = "/{id}")
  public void updateAlbum(@RequestBody Album album, @PathVariable Long id) {
    albumService.updateAlbum(album, id);
  }
}
