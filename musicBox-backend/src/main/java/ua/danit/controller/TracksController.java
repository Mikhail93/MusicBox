package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.model.Track;
import ua.danit.service.TrackService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/tracks")
public class TracksController {

  @Autowired
  private TrackService trackService;

  @GetMapping(path = "/")
  public List<Track> getAllTracks() {
    return trackService.getAllTracks();
  }

  @GetMapping(path = "/{id}")
  public Track getTrackById(@PathVariable Long id) {
    return trackService.getTrackById(id);
  }

  @DeleteMapping(path = "/{id}")
  public void deleteTrack(@PathVariable Long id) {
    trackService.deleteTrack(id);
  }

  @PutMapping(value = "/{id}")
  public void updateTrack(@RequestBody Track track, @PathVariable Long id) {
    trackService.updateTrack(track, id);
  }
}
