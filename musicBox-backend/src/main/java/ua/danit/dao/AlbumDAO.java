package ua.danit.dao;

import org.springframework.data.repository.CrudRepository;
import ua.danit.model.Album;

import java.util.List;

public interface AlbumDAO {

  List<Album> getAllAlbums();

  Album getAlbumById(Long id);

  void deleteAlbum(Long id);

  void deleteCover(Long id);

  void updateAlbum(Album album, Long id);

}
