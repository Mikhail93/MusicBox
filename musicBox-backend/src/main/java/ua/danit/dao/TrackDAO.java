package ua.danit.dao;

import ua.danit.model.Track;

import java.util.List;

public interface TrackDAO {
  List<Track> getAllTracks();

  Track getTrackById(Long id);

  void deleteTrack(Long id);

  void updateTrack(Track track, Long id);
}
