package ua.danit.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.danit.model.Track;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Transactional
@Repository
public class TrackDAOImpl implements TrackDAO {

  @Autowired
  private EntityManager entityManager;

  @Override
  public List<Track> getAllTracks() {
    Query query = entityManager.createNativeQuery("SELECT * FROM TRACKS", Track.class);
    List<Track> list = (List<Track>) query.getResultList();
    return list;
  }

  @Override
  public Track getTrackById(Long id) {
    String sql = "SELECT * FROM TRACKS WHERE ID IN(" + id + ")";
    Query query = entityManager.createNativeQuery(sql, Track.class);
    Track track = (Track) query.getSingleResult();
    return track;
  }

  @Override
  public void deleteTrack(Long id) {
    Track track = entityManager.find(Track.class, id);
    entityManager.remove(track);
  }

  @Override
  public void updateTrack(Track track, Long id) {
    track.setId(id);
    entityManager.merge(track);
  }
}
