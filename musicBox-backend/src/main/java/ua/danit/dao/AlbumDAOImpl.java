package ua.danit.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.danit.model.Album;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional
@Repository
public class AlbumDAOImpl implements AlbumDAO {

  @Autowired
  private EntityManager entityManager;

  @Override
  public List<Album> getAllAlbums() {
    Query query = entityManager.createNativeQuery("SELECT * FROM ALBUMS", Album.class);
    List<Album> list = (List<Album>) query.getResultList();
    return list;
  }

  @Override
  public Album getAlbumById(Long id) {
    String sql = "SELECT * FROM ALBUMS WHERE ID IN(" + id + ")";
    Query query = entityManager.createNativeQuery(sql, Album.class);
    Album album = (Album) query.getSingleResult();
    return album;
  }

  @Override
  public void deleteAlbum(Long id) {
    Album album = entityManager.find(Album.class, id);
    entityManager.remove(album);
  }

  @Override
  public void deleteCover(Long id) {
    String sql = "UPDATE ALBUMS SET COVER = null WHERE ID IN(" + id + ")";
    Query query = entityManager.createNativeQuery(sql, Album.class);
    query.executeUpdate();
  }

  @Override
  public void updateAlbum(Album album, Long id) {
    album.setId(id);
    entityManager.merge(album);
  }
}
