package ua.danit.service;

import org.springframework.stereotype.Service;
import ua.danit.model.Album;

import java.util.List;

public interface AlbumService {

  List<Album> getAllAlbums();

  Album getAlbumById(Long id);

  void deleteAlbum(Long id);

  void deleteCover(Long id);

  void updateAlbum(Album album, Long id);

}
