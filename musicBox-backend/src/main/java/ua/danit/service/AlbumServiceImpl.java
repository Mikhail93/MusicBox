package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDAO;
import ua.danit.model.Album;

import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {

  @Autowired
  private AlbumDAO albumDAO;

  @Override
  public List<Album> getAllAlbums() {
    return albumDAO.getAllAlbums();
  }

  @Override
  public Album getAlbumById(Long id) {
    return albumDAO.getAlbumById(id);
  }

  @Override
  public void deleteAlbum(Long id) {
    albumDAO.deleteAlbum(id);
  }

  @Override
  public void deleteCover(Long id) {
    albumDAO.deleteCover(id);
  }

  @Override
  public void updateAlbum(Album album, Long id) {
    albumDAO.updateAlbum(album, id);
  }
}
