package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.TrackDAO;
import ua.danit.model.Track;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

  @Autowired
  private TrackDAO trackDAO;

  @Override
  public List<Track> getAllTracks() {
    return trackDAO.getAllTracks();
  }

  @Override
  public Track getTrackById(Long id) {
    return trackDAO.getTrackById(id);
  }

  @Override
  public void deleteTrack(Long id) {
    trackDAO.deleteTrack(id);
  }

  @Override
  public void updateTrack(Track track, Long id) {
    trackDAO.updateTrack(track, id);
  }
}
