package ua.danit.service;

import ua.danit.model.Track;

import java.util.List;

public interface TrackService {
  List<Track> getAllTracks();

  Track getTrackById(Long id);

  void deleteTrack(Long id);

  void updateTrack(Track track, Long id);
}
